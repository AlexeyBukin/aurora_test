import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:qsettings_plugin/qsettings_plugin.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String? _errorMessage;
  final _qsettingsPlugin = QsettingsPlugin();
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    controller = TextEditingController();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      await _qsettingsPlugin.init();
    } on PlatformException {
      _errorMessage = 'Failed to initialize QsettingsPlugin.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('QsettingsPlugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              if (_errorMessage != null) Text(_errorMessage!),
              StreamBuilder<String>(
                stream: _qsettingsPlugin.onDataChanged(),
                builder: (_, snapshot) =>
                    Text(snapshot.data ?? snapshot.error?.toString() ?? ''),
              ),
              TextField(controller: controller),
              TextButton(
                child: const Text('Save'),
                onPressed: () async {
                  try {
                    await _qsettingsPlugin.saveData(controller.text);
                  } on PlatformException {
                    setState(() {
                      _errorMessage = 'QsettingsPlugin failed.';
                    });
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
