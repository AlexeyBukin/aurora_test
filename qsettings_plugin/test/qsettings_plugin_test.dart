import 'package:flutter_test/flutter_test.dart';
import 'package:qsettings_plugin/qsettings_plugin.dart';
import 'package:qsettings_plugin/qsettings_plugin_platform_interface.dart';
import 'package:qsettings_plugin/qsettings_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockQsettingsPluginPlatform
    with MockPlatformInterfaceMixin
    implements QsettingsPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final QsettingsPluginPlatform initialPlatform = QsettingsPluginPlatform.instance;

  test('$MethodChannelQsettingsPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelQsettingsPlugin>());
  });

  test('getPlatformVersion', () async {
    QsettingsPlugin qsettingsPlugin = QsettingsPlugin();
    MockQsettingsPluginPlatform fakePlatform = MockQsettingsPluginPlatform();
    QsettingsPluginPlatform.instance = fakePlatform;

    expect(await qsettingsPlugin.getPlatformVersion(), '42');
  });
}
