import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'qsettings_plugin_platform_interface.dart';

/// An implementation of [QsettingsPluginPlatform] that uses method channels.
class MethodChannelQsettingsPlugin extends QsettingsPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('qsettings_plugin');

  @override
  Future<void> saveData(String data) => methodChannel.invokeMethod<String>('saveData', data);

  @override
  Future<String?> loadData() => methodChannel.invokeMethod<String>('loadData');
}
