import 'dart:async';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'qsettings_plugin_method_channel.dart';

abstract class QsettingsPluginPlatform extends PlatformInterface {
  /// Constructs a QsettingsPluginPlatform.
  QsettingsPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static QsettingsPluginPlatform _instance = MethodChannelQsettingsPlugin();

  /// The default instance of [QsettingsPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelQsettingsPlugin].
  static QsettingsPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [QsettingsPluginPlatform] when
  /// they register themselves.
  static set instance(QsettingsPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<void> saveData(String data) {
    throw UnimplementedError('saveData() has not been implemented.');
  }

  Future<String?> loadData() {
    throw UnimplementedError('loadData() has not been implemented.');
  }
}
