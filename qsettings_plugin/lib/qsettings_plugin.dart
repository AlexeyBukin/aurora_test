import 'dart:async';

import 'qsettings_plugin_platform_interface.dart';

// Before using, [init] must be called.
class QsettingsPlugin {
  final StreamController<String> _controller = StreamController<String>();

  Stream<String> onDataChanged() => _controller.stream;

  Future<void> saveData(String data) async {
    await QsettingsPluginPlatform.instance.saveData(data);
    _controller.add(data);
  }

  Future<void> init() async {
    final initialData = await QsettingsPluginPlatform.instance.loadData();
    _controller.add(initialData ?? '');
  }
}
