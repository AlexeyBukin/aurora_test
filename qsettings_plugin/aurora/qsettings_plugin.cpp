#include <qsettings_plugin/qsettings_plugin.h>
#include <flutter/method-channel.h>
#include <QtCore>

void QsettingsPlugin::RegisterWithRegistrar(PluginRegistrar &registrar)
{
    registrar.RegisterMethodChannel("qsettings_plugin",
                                    MethodCodecType::Standard,
                                    [this](const MethodCall &call) { this->onMethodCall(call); });
}

void QsettingsPlugin::onMethodCall(const MethodCall &call)
{
    const auto &method = call.GetMethod();

    if (method == "saveData") {
        onSaveData(call);
        return;
    } else if (method == "loadData") {
        onLoadData(call);
        return;
    }

    unimplemented(call);
}

void QsettingsPlugin::onSaveData(const MethodCall &call)
{
    QSettings settings("data_settings", QSettings::NativeFormat);
    settings.beginGroup("settings");
    auto argument = call.GetArguments()[0];
    auto str = std::get<std::string>(argument);
    settings.setValue("data", QString(str.c_str()));
    settings.endGroup();

    call.SendSuccessResponse(nullptr);
}

void QsettingsPlugin::onLoadData(const MethodCall &call)
{
    QSettings settings ("data_settings", QSettings::NativeFormat);
    settings.beginGroup("settings");
    QString data = settings.value("data", QString("")).toString();
    settings.endGroup();

    call.SendSuccessResponse(data.toStdString());
}

void QsettingsPlugin::unimplemented(const MethodCall &call)
{
    call.SendSuccessResponse(nullptr);
}

//#include "moc_sensors_plus_aurora_plugin.cpp"
