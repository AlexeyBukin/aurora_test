#ifndef FLUTTER_PLUGIN_QSETTINGS_PLUGIN_H
#define FLUTTER_PLUGIN_QSETTINGS_PLUGIN_H

#include <flutter/plugin-interface.h>
#include <qsettings_plugin/globals.h>

class PLUGIN_EXPORT QsettingsPlugin final : public PluginInterface
{
public:
    void RegisterWithRegistrar(PluginRegistrar &registrar) override;

private:
    void onMethodCall(const MethodCall &call);
    void onSaveData(const MethodCall &call);
    void onLoadData(const MethodCall &call);
    void unimplemented(const MethodCall &call);
};

#endif /* FLUTTER_PLUGIN_QSETTINGS_PLUGIN_H */
